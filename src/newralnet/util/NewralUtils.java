package newralnet.util;

import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 *	Util class containing several static convenience methods
 */
public class NewralUtils {
	
	public static Random r=new Random();
	
	/**
	 * Sleep withouth having to catch anything
	 * @param millis - How long to sleep
	 */
	public static void safeSleep(long millis){//Fuck you java
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return Gaussian distributed double with mean 0 and standard deviation 1
	 */
	public static double randGaussian(){
		return r.nextGaussian();
	}
	
	
	/**
	 * Creates a gaussian distributed matrix
	 * @param rowDimension
	 * @param columnDimension
	 * @return A rowDimension x columnDimension matrix containing Gaussian distributed doubles
	 */
	public static RealMatrix randGaussianMatrix(int rowDimension, int columnDimension){
		RealMatrix out=new Array2DRowRealMatrix(rowDimension, columnDimension);
		for(int r=0; r<rowDimension; r++){
			for(int c=0; c<columnDimension; c++){
				out.setEntry(r, c, randGaussian());
			}
		}
		return out;
	}
	
	/**
	 * Similar to <code>randGaussianMatrix(dimension, 1);</code>
	 * @param size - size of the desired vector
	 * @return A gaussian distributed vector with <code>size</code> entries
	 */
	public static RealVector randGaussianVector(int size){
		RealVector out=new ArrayRealVector(size);
		for(int i=0; i<size; i++)out.setEntry(i, randGaussian());
		return out;
	}
	
	/**
	 * 
	 * @param vec - vector containing the values to be exponentiated
	 * @param p - power
	 * @return a new vector containing the exponentiated values
	 */
	public static RealVector elementPow(RealVector vec, double p){
		RealVector outVec=new ArrayRealVector(vec.getDimension());
		for(int i=0; i<vec.getDimension(); i++)outVec.setEntry(i, Math.pow(vec.getEntry(i),p));
		return outVec;
	}
	
	/**
	 * 
	 * @param matr - matrix containing the values to be exponentiated
	 * @param p - power
	 * @return a new vector containing the exponentiated values
	 */
	public static RealMatrix elementPow(RealMatrix matr, double p){//Element-wise power
		RealMatrix outMatr=new Array2DRowRealMatrix(matr.getRowDimension(), matr.getColumnDimension());
		for(int i=0; i<outMatr.getColumnDimension(); i++)outMatr.setColumnVector(i, elementPow(matr.getColumnVector(i), p));
		return outMatr;
	}
	
	/**
	 * Takes a {@link NewralOperations} operator and applies it componentwiese to the input array
	 * @param operator - {@link NewralOperations} operator to be applied
	 * @param inputs - one or more arrays
	 * @return an array containing the result of the operation
	 */
	public static double[][] componentwiseOperation(NewralOperations operator, double[][]... inputs){
		int maxWidth=-1, maxHeight=-1, minWidth=Integer.MAX_VALUE, minHeight=Integer.MAX_VALUE;
		for(double[][] in:inputs){
			if(in.length>maxWidth)maxWidth=in.length;
			if(in[0].length>maxHeight)maxHeight=in[0].length;
			if(in.length<minWidth)minWidth=in.length;
			if(in[0].length<minHeight)minHeight=in[0].length;
		}
		if(maxWidth!=minWidth||maxHeight!=minHeight)throw new IllegalArgumentException("Input lenghts dont match");
		double[][] out=new double[maxWidth][maxHeight];
		for(int x=0; x<maxWidth; x++){
			for(int y=0; y<maxHeight; y++){
				double[] args=new double[inputs.length];
				for(int i=0; i<inputs.length; i++){
					args[i]=inputs[i][x][y];
				}
				out[x][y]=operator.operate(args);
			}
		}
		return out;
	}
	
	/**
	 * Same as <code>componentwiseOperation</code> but with matrices
	 */
	public static RealMatrix componentwiseOperation(NewralOperations operator, RealMatrix... inputs){
		double[][][] nIn=new double[inputs.length][][];
		for(int i=0; i<inputs.length; i++)nIn[i]=inputs[i].getData();
		return new Array2DRowRealMatrix(componentwiseOperation(operator, nIn));
	}
	
	/**
	 * Same as <code>componentwiseOperation</code> but with one-dimensional arrays
	 */
	public static double[] componentwiseVectorOperation(NewralOperations operator, double[]... inputs){
		int maxLenght=-1, minLenght=Integer.MAX_VALUE;
		for(double[] in:inputs){
			if(in.length>maxLenght)maxLenght=in.length;
			if(in.length<minLenght)minLenght=in.length;
		}
		if(maxLenght!=minLenght)throw new IllegalArgumentException("Input lengths dont match");
		double[] out=new double[maxLenght];
		for(int i=0; i<maxLenght; i++){
			double[] args=new double[inputs.length];
			for(int j=0; j<inputs.length; j++)args[j]=inputs[j][i];
			out[i]=operator.operate(args);
		}
		return out;
	}
	
	/**
	 * Calculates the sum of all elements contained in a matrix
	 * @param inp - matrix whose elements should be summed
	 * @return the sum of all elements contained in <code>inp</code>
	 */
	public static double elementSum(RealMatrix inp){
		double sum=0;
		for(int r=0; r<inp.getRowDimension(); r++){
			for(int c=0; c<inp.getColumnDimension(); c++){
				sum+=inp.getEntry(r, c);
			}
		}
		return sum;
	}
	
}
