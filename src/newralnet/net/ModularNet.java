package newralnet.net;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import newralnet.core.Cost;
import newralnet.core.Layer;
import newralnet.core.Optimizer;
import newralnet.core.StringSaveAndLoadable;
import newralnet.core.TrainableLayer;
import newralnet.layers.ConvolutionLayer;
import newralnet.layers.FullyConnectedLayer;
import newralnet.layers.PoolingLayer;
import newralnet.layers.ReLULayer;
import newralnet.optimizers.AdamOptimizer;
import newralnet.util.LayerGradient;

/**
 * A network which can be built from multiple (compatible) layers
 */
public class ModularNet{
	/**
	 * Scalar for the randomly initialized weights
	 */
	public static final double GAUSS_SCALE=1;
	/**
	 * Input row & column Dimensions
	 */
	int inRD, inCD;
	/**
	 * Array containing all (trainable and non-trainable) layers of the network
	 */
	public Layer[] layers;
	/**
	 * Array containing only the {@link TrainableLayer}s of the network (which are also contained in the <code>layers</code> array
	 */
	public TrainableLayer[] trainableLayers;
	/**
	 * The optimizer used to train the network
	 */
	public Optimizer optimizer;
	/**
	 * The cost function which the optimizer tries to minimize
	 */
	public Cost cost;
	
	RealMatrix output;
	
	/**
	 * Default constructor
	 * @param inRD - Row-Dimension of the inputs the network will receive
	 * @param inCD - Colums-Dimension of the inputs the network will receive
	 * @param initNet - Specifies whether the trainable parameters of the net should get initialized (<code>false</code> if you plan on initializing them yourself)
	 * @param optimizer - The {@link Optimizer} which the net will use to train its parameters
	 * @param cost - The {@link Cost} function which the optimizer will try to minimize
	 * @param layers - One or more {@link Layer}s which the Network will use
	 */
	public ModularNet(int inRD, int inCD, boolean initNet, Optimizer optimizer, Cost cost, Layer... layers) {
		this.inRD=inRD;
		this.inCD=inCD;
		this.layers=layers;
		this.optimizer=optimizer;
		this.cost=cost;
		if(initNet)initNet();
	}

	protected void initNet() {
		ArrayList<TrainableLayer> tLayerList=new ArrayList<>();
		for(int l=0; l<layers.length; l++){
			layers[l].initialize(getLayerOutColumnDimension(l-1), getLayerOutRowDimension(l-1));
			if(layers[l] instanceof TrainableLayer){
				TrainableLayer tl=(TrainableLayer)layers[l];
				tl.initializeTrainableParameters();
				tLayerList.add(tl);
			}
		}
		trainableLayers=tLayerList.toArray(new TrainableLayer[0]);
	}
	
	int getLayerOutColumnDimension(int layer){
		if(layer==-1)return inCD;
		else return layers[layer].getOutColumnDimension();
	}
	
	int getLayerOutRowDimension(int layer){
		if(layer==-1)return inRD;
		else return layers[layer].getOutRowDimension();
	}
	
	/**
	 * Passes the given {@link RealMatrix} through the net
	 * @param input - {@link RealMatrix} containing the inputs which are to be passed through the net
	 * @return The ouput of the last layer
	 */
	public RealMatrix feedForward(RealMatrix input){
		RealMatrix mem=input.copy();
		for(int l=0; l<layers.length; l++){
			mem=layers[l].passForward(mem);
		}
		output=mem;
		return mem;
	}
	
	/**
	 * Convenience method - creates a {@link RealMatrix} and redirects to the {@link RealMatrix} <code>feedForward</code>-Method
	 * @param input - Two-dimensional double array containing the inputs which are to be passed through the net
	 * @return The ouput of the last layer (as a {@link RealMatrix})
	 */
	public RealMatrix feedForward(double[][] input){
		return feedForward(new Array2DRowRealMatrix(input));
	}
	
	/**
	 * Calculates the error and backpropagates it back throught the net to calculate the gradients
	 * @param answer -  The correct label
	 * @return The Gradients of each {@link TrainableLayer} in a {@link LayerGradient} array
	 */
	public LayerGradient[] backpropagate(RealMatrix answer){
		if(cost==null)throw new IllegalStateException("No cost function specified");
		LayerGradient[] bpOut=new LayerGradient[trainableLayers.length];
		int bpOutIndex=bpOut.length-1;
		RealMatrix error=cost.costPrime(output, answer);
		for(int i=layers.length-1; i>=0; i--){
			if(layers[i] instanceof TrainableLayer){
				bpOut[bpOutIndex]=((TrainableLayer)layers[i]).calcBPOutput(error);
				bpOutIndex--;
			}
			if(i>0)error=layers[i].passBackward(error);
		}
		return bpOut;
	}

	/**
	 * Uses the {@link ModularNet}s {@link Optimizer} to update its trainable parameters (weights and biases)
	 * @param gradients - A {@link LayerGradient} array containing the gradients of each layer (commonly obtained by <code>backpropagate</code>)
	 */
	public void updateParameters(LayerGradient[] gradients){
		if(optimizer!=null){
			optimizer.optimize(trainableLayers, gradients);
			optimizer.decay(trainableLayers);
		}
	}

	/**
	 * Creates a deep copy of the net
	 * @return A deep copy of the net
	 */
	public ModularNet copy(){
		ArrayList<TrainableLayer> tLayerList=new ArrayList<>();
		Layer[] newLayers=new Layer[layers.length];
		for(int l=0; l<newLayers.length; l++){
			Layer newLayer=layers[l].copy();
			newLayers[l]=newLayer;
			if(newLayer instanceof TrainableLayer)tLayerList.add((TrainableLayer)newLayer);
		}
		Optimizer optCopy = optimizer==null?null:optimizer.copy();
		ModularNet copy=new ModularNet(inRD, inCD, false, optCopy, cost, newLayers);
		copy.trainableLayers=tLayerList.toArray(new TrainableLayer[0]);
		return copy;
	}

	/**
	 * Reads the parameters of this net from {@link File} f (May take a few seconds depending on the filesize)
	 * These {@link File}s are usually created using the <code>saveNet</code>-Method
	 * @param f - The {@link File} which contains the parameters
	 */
	public void loadNet(File f) {
		Scanner fin=null;
		try {
			fin=new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ArrayList<Layer> newLayers=new ArrayList<>();
		ArrayList<TrainableLayer> newTrainableLayers=new ArrayList<>();
		while(fin.hasNextLine()){
			String line=fin.nextLine();
			String[] parts=line.split(":");
			
			if(parts[0].equals("Adam")){//Well thats not nicely solved
				if(optimizer instanceof AdamOptimizer){
					((AdamOptimizer) optimizer).constructFromSaveString(parts[1]);
				}
			}else{
				Layer newLayer=null;
				if(parts[0].equals("ConvolutionLayer")){
					newLayer=new ConvolutionLayer(0, 0, 0, 0);
				}else if(parts[0].equals("FullyConnectedLayer")){
					newLayer=new FullyConnectedLayer(0);
				}else if(parts[0].equals("PoolingLayer")){
					newLayer=new PoolingLayer();
				}else if(parts[0].equals("ReLULayer")){
					newLayer=new ReLULayer();
				}
				
				newLayer.constructFromSaveString(parts[1]);
				newLayers.add(newLayer);
				if(newLayer instanceof TrainableLayer)newTrainableLayers.add((TrainableLayer)newLayer);
			}
		}
		fin.close();
		layers=newLayers.toArray(new Layer[0]);
		trainableLayers=newTrainableLayers.toArray(new TrainableLayer[0]);
	}

	/**
	 * Writes the net's current state to {@link File} f (May take a few seconds depending on the size of the net)<br>
	 * The {@link File} then can be read again by the <code>loadNet</code>-Method
	 * @param f - {@link File} to write to
	 */
	public void saveNet(File f) {
		StringBuilder out=new StringBuilder();
		for(int l=0; l<layers.length; l++){
			out.append(layers[l].getClass().getSimpleName()+":");
			out.append(layers[l].getSaveString());
			if(l<layers.length-1)out.append("\n");
		}
		if(optimizer instanceof StringSaveAndLoadable){
			out.append("\n");
			out.append(optimizer.getClass().getSimpleName()+":");
			out.append(((StringSaveAndLoadable) optimizer).getSaveString());
		}
		
		try {
			FileWriter outWriter=new FileWriter(f);
			outWriter.write(out.toString());
			outWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
