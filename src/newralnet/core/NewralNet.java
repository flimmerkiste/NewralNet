package newralnet.core;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import org.apache.commons.math3.linear.RealMatrix;

import newralnet.display.BraInsight;
import newralnet.net.ModularNet;
import newralnet.util.LayerGradient;

/**
 * Core class containing some utility methods for working with {@link ModularNet}s
 */
public class NewralNet {
	/**
	 * The default amount of threads which should be used to train a {@link ModularNet}
	 */
	public static final int DEFAULT_WORKER_THREADS=1;
	
	/**
	 * {@link BufferedImage} containing the icon of the {@link BraInsight} Window
	 */
	public static BufferedImage icon;
	
	static {
		try {
			icon=ImageIO.read(NewralNet.class.getResourceAsStream("/Icon.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Takes an array of inputs and returns the result of feed-forwarding them through the {@link ModularNet} as an array
	 * @param inputs - A {@link RealMatrix} array containing the inputs for the {@link ModularNet}
	 * @param net - The {@link ModularNet} which should be used to make the predictions
	 * @return A {@link RealMatrix} array containing the predictions of the modular net
	 */
	//TODO Multithreading Predict
	public static RealMatrix[] predict(RealMatrix[] inputs, ModularNet net){
		RealMatrix[] predictions = new RealMatrix[inputs.length];
		for(int i=0; i<inputs.length; i++){
			predictions[i]=net.feedForward(inputs[i]);
		}
		return predictions;
	}
	
	/**
	 * Redirects to the main <code>train</code> method with the <code>workerThreads</code> argument = <code>DEFAULT_WORKER_THREADS</code>
	 * @param inputs - A {@link RealMatrix} array containing all inputs on which the {@link ModularNet} should be trained in this training step
	 * @param labels - A {@link RealMatrix} array containing the correct labels for the <code>inputs</code>
	 * @param net - The {@link ModularNet} which should be trained
	 */
	public static void train(RealMatrix[] inputs, RealMatrix[] labels, ModularNet net){
		train(inputs, labels, net, DEFAULT_WORKER_THREADS);
	}
	
	/**
	 * Executes a single training step (Calculating gradients and updating the trainable parameters accordingly)
	 * @param inputs - A {@link RealMatrix} array containing all inputs on which the {@link ModularNet} should be trained in this training step
	 * @param labels - A {@link RealMatrix} array containing the correct labels for the <code>inputs</code>
	 * @param net - The {@link ModularNet} which should be trained
	 * @param workerThreads - The amount of threads which should be used for this training step
	 */
	public static void train(RealMatrix[] inputs, RealMatrix[] labels, ModularNet net, int workerThreads){
		if(workerThreads == 0)workerThreads=Runtime.getRuntime().availableProcessors();
		if(inputs.length != labels.length)throw new IllegalArgumentException("Number of inputs and lables not equal");
		
		ExecutorService executor = Executors.newFixedThreadPool(workerThreads);
		RealMatrix[][] inputParts=new RealMatrix[workerThreads][];
		RealMatrix[][] labelParts=new RealMatrix[workerThreads][];
		int remainingPairs=inputs.length; //Remaining (unassigned) input-label pairs
		int assignedPairs=0;
		BrainWorker[] workers=new BrainWorker[workerThreads];
		
		//Assign input-label pairs to workers
		for(int i=0; i<workerThreads; i++){
			int partLenght=remainingPairs/(workerThreads-i);//Number of input-label pairs for current worker
			inputParts[i]=new RealMatrix[partLenght];
			labelParts[i]=new RealMatrix[partLenght];
			remainingPairs-=partLenght;
			for(int b=0; b<partLenght; b++){
				inputParts[i][b]=inputs[assignedPairs+b];
				labelParts[i][b]=labels[assignedPairs+b];
			}
			assignedPairs+=partLenght;
			
			//Calculate Gradients for pairs
			workers[i]=new BrainWorker(inputParts[i], labelParts[i], net);
			executor.execute(workers[i]);
		}
		
		//Wait for all workers to terminate
		executor.shutdown();
		while(!executor.isTerminated());
		
		//Collect and sum gradients from workers
		LayerGradient[] layerGradients = null;
		for(BrainWorker bw:workers){
			for(int g=0; g<bw.gradientParts.length; g++){
				if(layerGradients == null){
					layerGradients = bw.gradientParts[g]; //First assignment
					continue;
				}
				for(int l=0; l<bw.gradientParts[0].length; l++){
					//Add to existing gradients
					layerGradients[l].add(bw.gradientParts[g][l]);
				}
			}
		}
		
		//Averaging Gradients
		for(int l=0; l<layerGradients.length; l++){
			layerGradients[l]=layerGradients[l].scale(1d/inputs.length);
		}
		
		net.updateParameters(layerGradients);
	}
	
	static private class BrainWorker implements Runnable{
		RealMatrix[] inputs, labels;
		LayerGradient[][] gradientParts;
		ModularNet brain;
		
		public BrainWorker(RealMatrix[] inputs, RealMatrix[] labels, ModularNet brain) {
			this.inputs=inputs;
			this.labels=labels;
			this.brain=brain.copy();//Thread safe
			gradientParts=new LayerGradient[inputs.length][];
		}

		@Override
		public void run() {
			for(int i=0; i<inputs.length; i++){
				gradientParts[i]=calculateGradients(inputs[i], labels[i]);
			}
		}
		
		LayerGradient[] calculateGradients(RealMatrix input, RealMatrix label){
			brain.feedForward(input); //Prepare Brain
			return brain.backpropagate(label);
		}
		
	}
	
}
