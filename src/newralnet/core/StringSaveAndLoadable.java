package newralnet.core;

import newralnet.net.ModularNet;

/**
 * An interface which guarantees that the implementing class can be saved to and loaded from a {@link String} <br>
 * Used to save information about the {@link Layer}s of a {@link ModularNet} into a file.
 */
public interface StringSaveAndLoadable {
	/**
	 * @return A {@link String} from which the object can later be reconstructed from using <code>constructFromSaveString</code>
	 */
	public abstract String getSaveString();
	/**
	 * Reconstructs the object from a {@link String}
	 * @param str - A {@link String} which was obtained by calling <code>getSaveString</code>
	 */
	public abstract void constructFromSaveString(String str);
}
