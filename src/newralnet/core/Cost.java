package newralnet.core;

import org.apache.commons.math3.linear.RealMatrix;

import newralnet.net.ModularNet;

/**
 * An abstract class describing a cost function to be used with a {@link ModularNet}
 */
public abstract class Cost {
	/**
	 * Calculates the cost of the net's predictions and the correct answers
	 * @param predicted - the prediction of the {@link ModularNet}
	 * @param label - the correct answer
	 * @return The cost of the net's predictions and the correct answers
	 */
	public abstract double cost(RealMatrix predicted, RealMatrix label);
	/**
	 * Calculates the derivative of the cost function with respect to the nets predictions (= the output of its final layer)
	 * @param predicted - The prediction of the {@link ModularNet} (= the output of its final layer)
	 * @param label - The correct answer
	 * @return The derivative of the cost function with respect to the nets predictions
	 */
	public abstract RealMatrix costPrime(RealMatrix predicted, RealMatrix label);
}
